# configservice
Spring Boot Config server features:
* (secured )REST API for config access
* Encryption of configuration values
* Configuration properties can be stored in:
  * Local filesystem
  * GIT/SVN repository
  * HashiCorp Valut (https://www.vaultproject.io/)
  * JDBC
  * You can combine several config back-ends
 * Alternatively you can use HashiCorp Consul (https://www.consul.io/) as centralised configuration `spring-cloud-starter-consul-config` 
  
## Try it  
  
* `GET http://localhost:8888/{service_name}/{profile}/{label}/`
* Current development properties for the castle service: `curl -u user:itexperts http://localhost:8888/castle/default/development`
* To encrypt config value use `curl -X POST http://localhost:8888/encrypt -d password`

## Secure access

* Use Spring Security to configure security for Config Server API
* `compile ('org.springframework.boot:spring-boot-starter-security')`
* For HTTP Basic Auth configure password in _application.properties_ 
